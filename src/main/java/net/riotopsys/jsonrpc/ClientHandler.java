package net.riotopsys.jsonrpc;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by adam on 6/21/17.
 */
public class ClientHandler {

    private final Gson gson;

    private long counter = 0;

    private Map<Long, ClientFuture<?>> waitingFutures = new HashMap<>();
    protected BlockingQueue<String> outgoingMessages = new LinkedBlockingQueue<>();


    public ClientHandler(Gson gson) {
        this.gson = gson;
    }

    @SuppressWarnings("unchecked")
    public <T> T createInterface(Class<T> clazz){
        return (T) Proxy.newProxyInstance(getClass().getClassLoader(),
                new Class[]{clazz},
                new ClientInvocationHandler()
        );
    }

    public String getNextOutput() throws InterruptedException {
        return outgoingMessages.take();
    }

    @SuppressWarnings("unchecked")
    public void updatefromSever(String result){
        JsonResult jsonResult = gson.fromJson(result, JsonResult.class);

        interalUpdate(jsonResult);
    }

    private void interalUpdate(JsonResult jsonResult) {
        ClientFuture future = waitingFutures.get(jsonResult.getId().getAsLong());

        future.put(gson.fromJson(jsonResult.getResult(), future.getType()));
    }

    public void updatefromSever(JsonObject input) {
        JsonResult jsonResult = gson.fromJson(input, JsonResult.class);

        interalUpdate(jsonResult);
    }

    private class ClientInvocationHandler implements InvocationHandler {
        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

            Request anno = method.getAnnotation(Request.class);
            if ( anno == null){
                throw new IllegalArgumentException("method not annotated with @Request");
            }

            if ( method.getReturnType() == void.class ){
                JsonRequest request = new JsonRequest(null,
                        anno.name(),
                        gson.toJsonTree(args) );

                output( request );

                return null;

            } else if ( method.getReturnType() == Future.class){
                long id = counter++;

                JsonRequest request = new JsonRequest(gson.toJsonTree(id).getAsJsonPrimitive(),
                        anno.name(),
                        gson.toJsonTree(args) );

                output(request);

                Type returnType = ((ParameterizedType) method.getGenericReturnType()).getActualTypeArguments()[0];

                ClientFuture future = new ClientFuture(returnType);

                waitingFutures.put(id, future);

                return future;
            } else {
                throw new IllegalArgumentException("return types must be void or Future");
            }
        }
    }

    private void output(Object obj) {
        outgoingMessages.add( gson.toJson(obj) );
    }
}
