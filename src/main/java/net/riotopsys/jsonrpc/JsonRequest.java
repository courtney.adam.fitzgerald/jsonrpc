package net.riotopsys.jsonrpc;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.annotations.SerializedName;

/**
 * Created by adam on 6/20/17.
 */
public class JsonRequest {
    @SerializedName("jsonrpc")
    private String jsonrpc = "2.0";

    @SerializedName("method")
    private String method;

    @SerializedName("params")
    private JsonElement params;

    @SerializedName("id")
    private JsonPrimitive id;

    public JsonRequest(JsonPrimitive id, String method, JsonElement params) {
        this.id = id;
        this.method = method;
        this.params = params;
    }

    public String getJsonrpc() {
        return jsonrpc;
    }

    public String getMethod() {
        return method;
    }

    public JsonElement getParams() {
        return params;
    }

    public JsonPrimitive getId() {
        return id;
    }
}
