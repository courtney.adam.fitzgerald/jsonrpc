package net.riotopsys.jsonrpc;

import com.google.gson.JsonPrimitive;
import com.google.gson.annotations.SerializedName;

/**
 * Created by adam on 6/20/17.
 */
public class JsonError {

    public static final InternalError PARSE_ERROR = new InternalError(-32700, "Parse error");
    public static final InternalError METHOD_NOT_FOUND = new InternalError(-32601, "Method not found");
    public static final InternalError INVALID_PARAMS = new InternalError(-32602, "Invalid Params");
    public static final InternalError INVALID_REQUEST = new InternalError(-32600, "Invalid Request");
    public static final InternalError UNIMPLEMENTED = new InternalError(-32000, "Server error: Unimplemented");

    public JsonError(InternalError error) {
        this.id = null;
        this.error = error;
    }

    public JsonError(JsonPrimitive id, int code, String message) {
        this.id = id;
        error = new InternalError(code, message);
    }

    public JsonError(JsonPrimitive id, InternalError error) {
        this.id = id;
        this.error = error;
    }

    public static class InternalError{

        public InternalError(int code, String message) {
            this.code = code;
            this.message = message;
        }

        @SerializedName("code")
        private int code;

        @SerializedName("message")
        private String message;

        public int getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }
    }

    @SerializedName("jsonrpc")
    private String jsonrpc = "2.0";

    @SerializedName("error")
    private InternalError error;

    @SerializedName("id")
    private JsonPrimitive id;

    public String getJsonrpc() {
        return jsonrpc;
    }

    public InternalError getError() {
        return error;
    }

    public JsonPrimitive getId() {
        return id;
    }
}
