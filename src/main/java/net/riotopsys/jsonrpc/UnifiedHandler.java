package net.riotopsys.jsonrpc;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by adam on 6/22/17.
 */
public class UnifiedHandler<T> {

    private final ServerHandler<T> serverHandler;
    private final ClientHandler clientHandler;
    private Gson gson;

    private final BlockingQueue<String> outgoingMessages = new LinkedBlockingQueue<>();

    public UnifiedHandler(T serverImplementation, Gson gson) {
        this.serverHandler = new ServerHandler<>(serverImplementation,gson);
        this.clientHandler = new ClientHandler(gson);
        this.gson = gson;

        serverHandler.outgoingMessages = outgoingMessages;
        clientHandler.outgoingMessages = outgoingMessages;
    }

    public <C> C createClientInterface(Class<C> clazz){
        return clientHandler.createInterface(clazz);
    }

    public void onMessage( String json ){
        JsonElement input = gson.fromJson(json, JsonElement.class);

        if ( input.isJsonArray()) {
            serverHandler.onMessage(input);
        } if ( input.isJsonObject() && input.getAsJsonObject().has("method" )){
            serverHandler.onMessage(input);
        } else {
            clientHandler.updatefromSever(input.getAsJsonObject());
        }
    }

    public String getNextOutput() throws InterruptedException {
        return outgoingMessages.take();
    }


}
