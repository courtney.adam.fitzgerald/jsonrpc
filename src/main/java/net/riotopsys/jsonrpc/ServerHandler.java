package net.riotopsys.jsonrpc;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by adam on 6/20/17.
 */
public class ServerHandler<T> {

    private final Object implementation;
    private final Gson gson;

    protected BlockingQueue<String> outgoingMessages = new LinkedBlockingQueue<>();

    private Map<String, Method> nameLookup = new HashMap<>();

    public ServerHandler(T implementation, Gson gson) {
        this.implementation = implementation;
        this.gson = gson;
        Class<?> clazz = implementation.getClass();
        Arrays.stream(clazz.getMethods()).forEach(method -> {
            Request anno = method.getAnnotation(Request.class);
            if ( anno != null){
                nameLookup.put(anno.name(), method);
            }
        });
    }

    public void onMessage( String json ){
        onMessage(gson.fromJson(json, JsonElement.class));
    }

    public void onMessage(JsonElement json) {
        try {
            output( processRequest(json) );
        } catch ( JsonParseException e) {

            try {
                List<JsonRequest> requests = gson.fromJson(json, new TypeToken<List<JsonRequest>>() {}.getType());

                List<Object> results = new LinkedList<>();

                for ( JsonRequest request: requests){
                    Object result = processRequest(gson.toJsonTree(request));
                    if ( result != null ){
                        results.add(result);
                    }
                }

                if ( !results.isEmpty() ){
                    output(results);
                }

            } catch (JsonParseException e2) {
                output(new JsonError(JsonError.PARSE_ERROR));
            }
        }

    }

    private Object processRequest(JsonElement json) throws JsonParseException{
        JsonRequest request = gson.fromJson(json, JsonRequest.class);

        Method method = nameLookup.get(request.getMethod());
        if ( method == null ){
            return new JsonError(JsonError.METHOD_NOT_FOUND);
        }

        JsonElement rawParams = request.getParams();

        if ( rawParams.isJsonArray() ){
            JsonArray array  = rawParams.getAsJsonArray();

            Type[] paramTypes = method.getGenericParameterTypes();
            Object[] finalParams = new Object[paramTypes.length];

            try {
                for (int i = 0; i < paramTypes.length; i++) {
                    finalParams[i] = gson.fromJson(array.get(i), paramTypes[i]);
                }
            } catch (JsonSyntaxException e){
                return new JsonError(request.getId(), JsonError.INVALID_PARAMS);
            }

            try {
                Object result = method.invoke(implementation, finalParams);
                if ( method.getReturnType() != void.class ){
                    return new JsonResult(request.getId(), gson.toJsonTree(result));
                } else {
                    return null;
                }
            }  catch (Exception e) {
                e.printStackTrace();
                return new JsonError(request.getId(), JsonError.INVALID_REQUEST);
            }


        } else{
            return new JsonError(request.getId(), JsonError.UNIMPLEMENTED);
        }
    }


    private void output(Object obj) {
        if ( obj == null ) {
            return;
        }
        outgoingMessages.add( gson.toJson(obj));
    }

    public String getNextOutput() throws InterruptedException {
        return outgoingMessages.take();
    }


}
