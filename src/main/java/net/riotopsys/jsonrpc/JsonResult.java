package net.riotopsys.jsonrpc;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.annotations.SerializedName;

/**
 * Created by adam on 6/20/17.
 */
public class JsonResult {
    public JsonResult(JsonPrimitive id, JsonElement result) {
        this.id = id;
        this.result = result;
    }

    @SerializedName("jsonrpc")
    private String jsonrpc = "2.0";

    @SerializedName("result")
    private JsonElement result;

    @SerializedName("id")
    private JsonPrimitive id;

    public JsonPrimitive getId() {
        return id;
    }

    public JsonElement getResult() {
        return result;
    }
}
