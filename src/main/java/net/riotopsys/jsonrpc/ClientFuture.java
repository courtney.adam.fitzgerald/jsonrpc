package net.riotopsys.jsonrpc;

import java.lang.reflect.Type;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by adam on 6/20/17.
 */
public class ClientFuture<T> implements Future<T> {
    private final CountDownLatch latch = new CountDownLatch(1);
    private T value;
    private Type type;

    public ClientFuture(Type type) {
        this.type = type;
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        return false;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public boolean isDone() {
        return latch.getCount() == 0;
    }

    @Override
    public T get() throws InterruptedException, ExecutionException {
        latch.await();
        return value;
    }

    @Override
    public T get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        if ( latch.await(timeout,unit) ){
            return value;
        } else {
            throw new TimeoutException();
        }
    }

    public void put(T value) {
        this.value = value;
        latch.countDown();
    }

    public Type getType() {
        return type;
    }
}
