package net.riotopsys.jsonrpc;

import com.google.gson.GsonBuilder;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Created by adam on 6/20/17.
 */
public class ServerHandlerTest {

    private static class SimpleInterface{

        public boolean wasCalled = false;
        public String lastMessage;

        @Request(name="add")
        public int onAdd( int a, int b){
            return a+b;
        }

        @Request(name="notice")
        public void onNotice( String message ){
            lastMessage = message;
            wasCalled = true;
        }

    }


    @Test
    public void canMakeSingleRequestWithOutput(){

        ServerHandler<SimpleInterface> dut = new ServerHandler<>(new SimpleInterface(), new GsonBuilder().setPrettyPrinting().create());

        dut.onMessage("{\"jsonrpc\": \"2.0\", \"method\": \"add\", \"params\": [23, 42], \"id\": 2}");

        try {
            String result = dut.getNextOutput();
            assertThat(result, is("{\n" +
                    "  \"jsonrpc\": \"2.0\",\n" +
                    "  \"result\": 65,\n" +
                    "  \"id\": 2\n" +
                    "}"));
        } catch (InterruptedException e) {
            fail();
        }

    }

    @Test
    public void canMakeSingleRequestWithOutOutput(){

        SimpleInterface bob = new SimpleInterface();
        ServerHandler<SimpleInterface> dut = new ServerHandler<>(bob, new GsonBuilder().setPrettyPrinting().create());

        dut.onMessage("{\"jsonrpc\": \"2.0\", \"method\": \"notice\", \"params\": [\"stuff happend\"]}");
        dut.onMessage("{\"jsonrpc\": \"2.0\", \"method\": \"add\", \"params\": [23, 42], \"id\": 2}");

        try {
            String result = dut.getNextOutput();
            assertThat(result, is("{\n" +
                    "  \"jsonrpc\": \"2.0\",\n" +
                    "  \"result\": 65,\n" +
                    "  \"id\": 2\n" +
                    "}"));
            assertThat(bob.wasCalled, is(true));
            assertThat(bob.lastMessage, is("stuff happend"));
        } catch (InterruptedException e) {
            fail();
        }

    }

    @Test
    public void canMakeBatchRequestWithOutOutput(){

        SimpleInterface bob = new SimpleInterface();
        ServerHandler<SimpleInterface> dut = new ServerHandler<>(bob, new GsonBuilder().setPrettyPrinting().create());

        dut.onMessage("[{\"jsonrpc\": \"2.0\", \"method\": \"notice\", \"params\": [\"stuff happend\"]}, " +
                "{\"jsonrpc\": \"2.0\", \"method\": \"add\", \"params\": [5, 7], \"id\": 1}," +
                "{\"jsonrpc\": \"2.0\", \"method\": \"add\", \"params\": [23, 42], \"id\": 2}]");

        try {
            String result = dut.getNextOutput();
            assertThat(result, is("[\n" +
                    "  {\n" +
                    "    \"jsonrpc\": \"2.0\",\n" +
                    "    \"result\": 12,\n" +
                    "    \"id\": 1\n" +
                    "  },\n" +
                    "  {\n" +
                    "    \"jsonrpc\": \"2.0\",\n" +
                    "    \"result\": 65,\n" +
                    "    \"id\": 2\n" +
                    "  }\n" +
                    "]"));
            assertThat(bob.wasCalled, is(true));
            assertThat(bob.lastMessage, is("stuff happend"));
        } catch (InterruptedException e) {
            fail();
        }

    }
}
