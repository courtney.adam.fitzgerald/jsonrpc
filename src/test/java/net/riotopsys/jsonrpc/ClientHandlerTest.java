package net.riotopsys.jsonrpc;

import com.google.gson.GsonBuilder;
import org.junit.Test;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Created by adam on 6/20/17.
 */
public class ClientHandlerTest {

    private interface SimpleInterface{

        @Request(name="add")
        Future<Integer> onAdd( int a, int b);

        @Request(name="notice")
        void onNotice( String message );

    }


    @Test
    public void canRequestWithFuture(){
        ClientHandler dut = new ClientHandler(new GsonBuilder().setPrettyPrinting().create());

        SimpleInterface si = dut.createInterface(SimpleInterface.class);

        Future<Integer> f = si.onAdd(5, 45);
        assertThat( f, is(notNullValue()));

        try {
            String result = dut.getNextOutput();
            assertThat(result, is("{\n" +
                    "  \"jsonrpc\": \"2.0\",\n" +
                    "  \"method\": \"add\",\n" +
                    "  \"params\": [\n" +
                    "    5,\n" +
                    "    45\n" +
                    "  ],\n" +
                    "  \"id\": 0\n" +
                    "}"));
            assertThat(f, is( instanceOf(ClientFuture.class)));
            assertThat(f.isDone(), is(false));
        } catch (InterruptedException e) {
            fail();
        }
    }

    @Test
    public void canRequestWithFutureCanComplete(){
        ClientHandler dut = new ClientHandler(new GsonBuilder().setPrettyPrinting().create());

        SimpleInterface si = dut.createInterface(SimpleInterface.class);

        Future<Integer> f = si.onAdd(5, 45);
        assertThat( f, is(notNullValue()));

        try {
            String result = dut.getNextOutput();
            assertThat(result, is("{\n" +
                    "  \"jsonrpc\": \"2.0\",\n" +
                    "  \"method\": \"add\",\n" +
                    "  \"params\": [\n" +
                    "    5,\n" +
                    "    45\n" +
                    "  ],\n" +
                    "  \"id\": 0\n" +
                    "}"));
            assertThat(f, is( instanceOf(ClientFuture.class)));
            assertThat(f.isDone(), is(false));
            dut.updatefromSever("{\n" +
                    "  \"jsonrpc\": \"2.0\",\n" +
                    "  \"result\": 50,\n" +
                    "  \"id\": 0\n" +
                    "}");
            assertThat(f.isDone(), is(true));
            assertThat(f.get(), is(50));
        } catch (InterruptedException | ExecutionException e) {
            fail();
        }
    }

    @Test
    public void canRequestWithoutFuture(){
        ClientHandler dut = new ClientHandler(new GsonBuilder().setPrettyPrinting().create());

        SimpleInterface si = dut.createInterface(SimpleInterface.class);

        si.onNotice("damn");

        try {
            String result = dut.getNextOutput();
            assertThat(result, is("{\n" +
                    "  \"jsonrpc\": \"2.0\",\n" +
                    "  \"method\": \"notice\",\n" +
                    "  \"params\": [\n" +
                    "    \"damn\"\n" +
                    "  ]\n" +
                    "}"));

        } catch (InterruptedException e) {
            fail();
        }
    }
}
