package net.riotopsys.jsonrpc;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Test;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Created by adam on 6/21/17.
 */
public class EndToEndTest {

    private static class SimpleImplementation{

        public boolean wasCalled = false;
        public String lastMessage;

        @Request(name="add")
        public int onAdd( int a, int b){
            return a+b;
        }

        @Request(name="notice")
        public void onNotice( String message ){
            lastMessage = message;
            wasCalled = true;
        }

    }
    private interface SimpleInterface{

        @Request(name="add")
        Future<Integer> onAdd(int a, int b);

        @Request(name="notice")
        void onNotice( String message );

    }

    @Test
    public void fullLoop(){

        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        ClientHandler clientHandler = new ClientHandler(gson);
        SimpleImplementation bob = new SimpleImplementation();
        ServerHandler<SimpleImplementation> serverHandler = new ServerHandler<>(bob, gson);

        Thread clientUpdater = new Thread(){
            @Override
            public void run() {
                while (true){
                    try {
                        clientHandler.updatefromSever(serverHandler.getNextOutput());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        Thread serverUpdater = new Thread(){
            @Override
            public void run() {
                while(true){
                    try {
                        serverHandler.onMessage(clientHandler.getNextOutput());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        clientUpdater.setDaemon(true);
        clientUpdater.start();

        serverUpdater.setDaemon(true);
        serverUpdater.start();

        SimpleInterface simpleInterface = clientHandler.createInterface(SimpleInterface.class);

        simpleInterface.onNotice("damn");

        Future<Integer> future = simpleInterface.onAdd(5, 17);

        assertThat( future.isDone(), is(false));
        assertThat( future, is(notNullValue()));
        assertThat( future, is( instanceOf(ClientFuture.class)));

        try {
            assertThat(future.get(), is(22));
        } catch (Exception e){
            fail();
        }

        assertThat( future.isDone(), is(true));
        assertThat(bob.wasCalled, is(true));
        assertThat(bob.lastMessage, is("damn"));

    }

}
