package net.riotopsys.jsonrpc;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Test;

import java.util.concurrent.Future;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Created by adam on 6/22/17.
 */
public class UnifiedHandlerTest {

    private static class SimpleImplementation{

        public boolean wasCalled = false;
        public String lastMessage;

        @Request(name="add")
        public int onAdd( int a, int b){
            return a+b;
        }

        @Request(name="notice")
        public void onNotice( String message ){
            lastMessage = message;
            wasCalled = true;
        }

    }
    private interface SimpleInterface{

        @Request(name="add")
        Future<Integer> onAdd(int a, int b);

        @Request(name="notice")
        void onNotice( String message );

    }

    @Test
    public void fullLoop(){

        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        SimpleImplementation bob = new SimpleImplementation();
        UnifiedHandler<SimpleImplementation> unifiedHandler = new UnifiedHandler<>(bob, gson);


        Thread updater = new Thread(){
            @Override
            public void run() {
                while (true){
                    try {
                        unifiedHandler.onMessage(unifiedHandler.getNextOutput());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        updater.setDaemon(true);
        updater.start();

        SimpleInterface simpleInterface = unifiedHandler.createClientInterface(SimpleInterface.class);

        simpleInterface.onNotice("damn");
        Future<Integer> future = simpleInterface.onAdd(5, 17);

        assertThat( future, is(notNullValue()));
        assertThat( future, is( instanceOf(ClientFuture.class)));
        assertThat( future.isDone(), is(false));

        try {
            assertThat(future.get(), is(22));
        } catch (Exception e){
            fail();
        }

        assertThat( future.isDone(), is(true));
        assertThat(bob.wasCalled, is(true));
        assertThat(bob.lastMessage, is("damn"));

    }
}
